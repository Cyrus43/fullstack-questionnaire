export interface SurveyModel extends Partial<any> {
  forms: [
    {
      id: number;
      created: string;
      modified: string;
      sort_order: number;
      path: string;
      depth: number;
      numchild: number;
      name: string;
      description: string;
      type: string;
      is_active: boolean;
      status: string;
      valid_from: string;
      valid_to: string;
      has_consent: boolean;
      is_primary: boolean;
      node_type: string;
      visibility: string;
      is_special: boolean;
      survey_airtime_compensation: number;
      survey_cash_compensation: number;
      survey_estimated_time: number;
      show_description: boolean;
      can_make_payments: boolean;
      can_track_data: boolean;
      pages: [
        {
          id: number;
          created: string;
          modified: string;
          sort_order: number;
          path: string;
          depth: number;
          numchild: number;
          name: string;
          description: string;
          type: string;
          is_active: boolean;
          status: string;
          valid_from: string;
          valid_to: string;
          has_consent: boolean;
          is_primary: boolean;
          node_type: string;
          visibility: string;
          is_special: boolean;
          survey_airtime_compensation: number;
          survey_cash_compensation: number;
          survey_estimated_time: number;
          show_description: boolean;
          can_make_payments: boolean;
          can_track_data: boolean;
          sections: [
            {
              id: number;
              created: string;
              modified: string;
              sort_order: number;
              path: string;
              depth: number;
              numchild: number;
              name: string;
              description: string;
              type: string;
              is_active: boolean;
              status: string;
              valid_from: string;
              valid_to: string;
              has_consent: boolean;
              is_primary: boolean;
              node_type: string;
              visibility: string;
              is_special: boolean;
              survey_airtime_compensation: number;
              survey_cash_compensation: number;
              survey_estimated_time: number;
              show_description: boolean;
              can_make_payments: boolean;
              can_track_data: boolean;
              questions: Question[];
            }
          ];
        }
      ];
      expected_views: 1;
      no_of_views: 0;
      gateway: 1;
      universe: 1;
      creator: {
        name: ' ';
        email: 'simonmuthusi@gmail.com';
      };
      airtime_compensation: 10.0;
      cash_compensation: 0.0;
      estimated_time: 0;
    }
  ];
  locations: [];
}

interface Question extends Partial<any> {
  id: number;
  created: string;
  modified: string;
  sort_order: number;
  type: string;
  is_mandatory: boolean;
  is_visible: boolean;
  text: string;
  description: string;
  detail: '';
  column_match: string;
  default: '';
  field_length: number;
  is_enabled: boolean;
  is_unique: boolean;
  has_skip: boolean;
  extras: {};
  is_active: boolean;
  error_message: string;
  validation_rule: string;
  is_unique_with: any[];
  widget: string;
  airtime_compensation: number;
  cash_compensation: number;
  estimated_time: number;
  show_if: any[];
  q_options: any[];
  uploads: any[];
}
