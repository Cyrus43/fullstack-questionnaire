/* eslint-disable no-undef */
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['../login/login.component.scss'],
})
export class SignupComponent implements OnInit {
  busy = false;
  name = '';
  email = '';
  username = this.email;
  password1 = '';
  password2 = '';
  phoneNumber = '';
  loginError = false;
  repeatPassword = false;
  private subscription: Subscription;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}
  signup() {
    this.busy = true;
    this.authService
      .signup(
        this.phoneNumber,
        this.name,
        this.email,
        this.password1,
        this.password2
      )
      .pipe(finalize(() => (this.busy = false)))
      .subscribe(
        () => {
          this.router.navigateByUrl('login');
        },
        (error) => {
          this.loginError = true;
          this._snackBar.open(
            localStorage.getItem('error') || 'an error has occured',
            'close',
            {
              duration: 1500,
              horizontalPosition: 'end',
            }
          );
          console.log(error);
        }
      );
  }
}
